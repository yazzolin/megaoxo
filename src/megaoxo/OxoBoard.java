package megaoxo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

@SuppressWarnings("serial")
public class OxoBoard extends Board{
	
	public OxoBoard(){}
	public void paintComponent(Graphics g){
		sizeX=View.WIDTH-26;
		sizeY=View.HEIGHT-149;
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, sizeX, sizeY);
		//System.out.println("grid to be drawn");
		drawGrid(g);
		//System.out.println("pawns to be drawn");
		drawPawns(g);
		//System.out.println("log to be drawn");
		drawLog(g);
		drawUndoButton(g);
	}
	private void drawGrid(Graphics g) {
		Graphics2D g2= (Graphics2D) g;
		g.setColor(Color.BLACK);
		for(int i=0;i<2;i++){
			for(int j=0;j<=15;j++){
				if(i==0){
					g2.drawLine(10, 10+j*(sizeY/15), sizeX, 10+j*(sizeY/15));
				}
				else{
					g2.drawLine(10+j*(sizeX/15), 10, 10+j*(sizeX/15), sizeY);
				}
			}
		}
		g2.setStroke(new BasicStroke(3));
		g2.draw(new Line2D.Float(10, 10, sizeX, 10));
		g2.draw(new Line2D.Float(10, 10, 10, sizeY));
		g2.draw(new Line2D.Float(sizeX, 10, sizeX, sizeY));
		g2.draw(new Line2D.Float(10, sizeY, sizeX, sizeY));
	}
	private void drawUndoButton(Graphics g){
		int x=sizeX-20;
		int y=sizeY+20;
		int size=20;
		Graphics2D g2 = (Graphics2D) g;
		g.setColor(Color.RED);
		g.drawRect(x, y, size, size);
		g2.setStroke(new BasicStroke(3));
		g2.draw(new Line2D.Float(x, y, x+size, y+size));
		g2.draw(new Line2D.Float(x+size, y, x, y+size));
		
	}
	@Override
	public String getNameOfTheGame() {
		return "OxO-Board";
	}
}
