package megaoxo;

import java.util.ArrayList;

public class GameOfOxo {

	private View window;
	private Grid grid= new OxoGrid();
	private ArrayList<Player> player= new ArrayList<Player>();
	private Player currentPlayer;
	private int numberOfPlayers;
	public static int flagPlayed=0;
	public static int winningFlag=0;
	private int starting=0;
	private int lineToWin;
	private int rounds=0;
	private ArrayList<int[]> playHistory = new ArrayList<int[]>();
	
	public GameOfOxo(View window,int numberOfPlayers){
		this.window=window;
		this.window.getBoard().setGrid(this.grid);
		this.numberOfPlayers=numberOfPlayers;
		if(numberOfPlayers==2){
			this.lineToWin=5;
			}
		else if(numberOfPlayers==3){
			this.lineToWin=4;
			}
	}
	
	public void start() {
		grid=new OxoGrid();
		window.getBoard().setGrid(grid);
		if(player.size()==0){
			for(int i=1;i<=numberOfPlayers;i++){
				player.add(new Player(0,i));
				//System.out.println("player "+i+" is created nb of player is: "+player.get(i-1).getNumber());
			}
		}
		currentPlayer=player.get(starting);
		starting=(starting+1)%numberOfPlayers;
	}

	public void clicAt(int clicX, int clicY) {
		//System.out.println("clic at x= "+clicX+" y= "+clicY);
		if(winningFlag==1){
			winningFlag=0;
			start();
			window.getBoard().repaint();
		}
		else{
			if(clicX<=View.WIDTH-26 && clicY<=View.HEIGHT-149 && clicX>=11 && clicY>=11){
				int x,y;
				x=Math.floorDiv(clicX-11,((View.WIDTH-26)/15));
				y=Math.floorDiv(clicY-11,((View.HEIGHT-149)/15));
				grid.setPawn(x,y,currentPlayer.getNumber());
				//System.out.println("new size: "+grid.getPawns().size());
				if(flagPlayed==1){
					window.getBoard().writeInLog("player " +getCurrentPlayer().getNumber()+" played");
					if(checkWin(x,y)){
						winGame(getCurrentPlayer());
					}
					int [] toadd={x,y};
					playHistory.add(toadd);
					rounds++;
					setCurrentPlayer(player.get((getCurrentPlayer().getNumber())%numberOfPlayers));
					updateBoard();
					flagPlayed=0;}
			}
			else if((clicX>=View.WIDTH-46 && clicX<=View.WIDTH-26 && clicY>=View.HEIGHT-129 && clicY<=View.HEIGHT-109) && playHistory.size()>0){
				//System.out.println("Undo!");
				//System.out.println("joueur num�ro "+ getCurrentPlayer().getNumber()+" ne joue pas");
				int previousNumber=getCurrentPlayer().getNumber()-1;
				if(previousNumber==0){
					previousNumber=numberOfPlayers;}
				//System.out.println("previous number = "+previousNumber);
				//window.getBoard().writeInLog("player " + (previousNumber) +" undid his move.");
				window.getBoard().writeInLog("player " + (previousNumber) +" is to play.");
				grid.removePawn(playHistory.get(rounds-1)[0], playHistory.get(rounds-1)[1]);
				playHistory.remove(rounds-1);
				setCurrentPlayer(player.get(previousNumber-1));
				updateBoard();
				rounds--;
			}
			else{
				System.out.println("out of boundaries");
			}
		}
	}
	private void updateBoard() {
		window.getBoard().setGrid(grid);
		window.getBoard().repaint();
	}

	public void setCurrentPlayer(Player newPlayer){
		currentPlayer=newPlayer;
	}
	public Player getCurrentPlayer(){
		return currentPlayer;
	}
	public boolean checkWin(int x, int y){
		//System.out.println("inside the checkWin");
		int count=1;
		for(short direction=0;direction<4;direction++){
			count=1;
			int newX=x;
			int newY=y;
			switch(direction){
			case 0: //W
				//System.out.println("direction west");
				for(int i=-1;i<=1;i=i+2){
					newX=x+i;
					while(grid.getPawnOwner(newX,newY)==getCurrentPlayer().getNumber()){
						count++;
						//System.out.println("count W = "+count);
						newX=newX+i;
					}
				}
				break;
			case 1: //NW
				//System.out.println("direction north-west");
				for(int i=-1;i<=1;i=i+2){
					newX=x+i;
					newY=y-i;
					while(grid.getPawnOwner(newX,newY)==getCurrentPlayer().getNumber()){
						count++;
						//System.out.println("count NW = "+count);
						newX=newX+i;
						newY=newY-i;
					}
				}
				break;
			case 2: //N
				//System.out.println("direction north");
				for(int i=-1;i<=1;i=i+2){
					newY=y+i;
					while(grid.getPawnOwner(newX,newY)==getCurrentPlayer().getNumber()){
						count++;
						//System.out.println("count N = "+count);
						newY=newY+i;
					}
				}
				break;
			case 3: //NE
				//System.out.println("direction north-east");
				for(int i=-1;i<=1;i=i+2){
					newX=x-i;
					newY=y-i;
					while(grid.getPawnOwner(newX,newY)==getCurrentPlayer().getNumber()){
						count++;
						//System.out.println("count NE = "+count);
						newX=newX-i;
						newY=newY-i;
					}
				}
				break;
			}
			if(count>=lineToWin){
				return true;
			}
		}
		return false;
	}
	public void winGame(Player winner){
		winner.won();
		window.getBoard().writeInLog("player "+ winner.getNumber()+" won! "+"He has "+winner.getWins()+" wins");
		winningFlag++;
	}
}
