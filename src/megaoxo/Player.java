package megaoxo;

public class Player {

	private int wins;
	private int number;
	
	public Player(){
		wins=0;
		number=1;
	}
	public Player(int wins, int number){
		this.wins=wins;
		this.number=number;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	public void won(){
		this.wins++;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
}
