package megaoxo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class TitleScreen extends Board {
	
	public static String game="OxO";
	
	public TitleScreen(){
		String [] gamelist ={"OxO","Chess"};
		JButton butt1=new JButton("Start game");
		JComboBox<String> combo= new JComboBox<String>(gamelist);
		butt1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				switch(TitleScreen.game){
					case "OxO":
						final JFrame window = new JFrame();
						try{
							int numberOfPlayer = Integer.parseInt(JOptionPane.showInputDialog(window,"enter a number of player: ",2));
							Laucher.launchOxo(numberOfPlayer);
						}
						catch (NumberFormatException except){/*System.out.println("button cancel pressed");*/}
						break;
				}
			}
		});
		this.add(butt1);
		combo.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				@SuppressWarnings("unchecked")
				JComboBox<String> combo = (JComboBox<String>) event.getSource();
				TitleScreen.game=(String) combo.getSelectedItem();
			}
		});
		this.add(combo);
	}
	
	public void paintComponent(Graphics g){
		text(g, "Welcome to the game", View.WIDTH/2-("Welcome to the game".length()/2)*(int)(25*0.57), View.HEIGHT/2, 25, Color.BLACK);
		
	}
	private void text(Graphics g, String text, int x, int y, int size, Color color){
		g.setFont(new Font("Courier", Font.PLAIN, size));
		g.setColor(color);
		g.drawString(text, x, y);
	}
	
	public String getNameOfTheGame() {
		return "Title-Screen";
	}
}
