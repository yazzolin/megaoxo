package megaoxo;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class Resize implements ComponentListener {

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		Dimension newSize = arg0.getComponent().getBounds().getSize();
		View.HEIGHT=newSize.height;
		View.WIDTH=newSize.width;
		//System.out.println("Height= "+View.HEIGHT+"Width= "+View.WIDTH);
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

}
