package megaoxo;

public class OxoGrid extends Grid{

	public OxoGrid(){
		super();
	}

	public void setPawn(int x, int y, int player) {
		if(getPawnOwner(x,y)==0 && GameOfOxo.flagPlayed==0){
			int[]tableau={x,y,player};
			grid.add(tableau);
			GameOfOxo.flagPlayed=1;
		}
	}
}
