package megaoxo;

import java.util.ArrayList;

public abstract class Grid {

	protected ArrayList<int[]> grid = new ArrayList<int[]>();

	public Grid() {
		super();
	}
	
	public abstract void setPawn(int x, int y, int player);

	public int getPawnOwner(int x, int y) {
		for(int i=0;i<grid.size();i++){
			if(grid.get(i)[0]==x && grid.get(i)[1]==y){
				return grid.get(i)[2];
			}
		}
		return 0;
	}

	public void removePawn(int x, int y) {
		for(int i=0;i<grid.size();i++){
			if(grid.get(i)[0]==x && grid.get(i)[1]==y){
				grid.remove(i);
			}
		}
	}

	public ArrayList<int[]> getPawns() {
		return grid;
	}

}