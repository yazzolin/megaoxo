package megaoxo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class Board extends JPanel {

	protected int sizeX = View.WIDTH-26;
	protected int sizeY = View.HEIGHT-149;
	private ArrayList<String> log = new ArrayList<String>();
	private int logLineCount = 0;

	Grid grid;
	public static int LINECOUNT = 6;

	public Board() {
		super();
	}

	public Board(LayoutManager layout) {
		super(layout);
	}

	public Board(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
	}

	public Board(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}

	protected void drawPawns(Graphics g) {
		//System.out.println("the size of the grid to draw: "+grid.getPawns().size());
		for(int i=0; i<grid.getPawns().size();i++){
			//System.out.println("element " +i +": x= " +grid.getPawns().get(i)[0]+" y= "+grid.getPawns().get(i)[1]+" player = "+grid.getPawns().get(i)[2]);
			if(grid.getPawns().get(i)[2]==1){
				drawCross(g,grid.getPawns().get(i)[0],grid.getPawns().get(i)[1]);
			}
			else if(grid.getPawns().get(i)[2]==2){
				drawCircle(g,grid.getPawns().get(i)[0],grid.getPawns().get(i)[1]);
			}
			else if(grid.getPawns().get(i)[2]==3){
				drawTriangle(g,grid.getPawns().get(i)[0],grid.getPawns().get(i)[1]);
			}
		}
	}

	private void drawCross(Graphics g, int x, int y) {
		Graphics2D g2= (Graphics2D) g;
		x=11+x*(View.WIDTH-37)/15;
		y=11+y*(View.HEIGHT-160)/15;
		g2.setStroke(new BasicStroke(3));
		g2.draw(new Line2D.Float(x+3, y+3, x+sizeX/15-3, y+sizeY/15-3));
		g2.draw(new Line2D.Float(x+3, y+sizeY/15-3, x+sizeX/15-3, y+3));
		//g.drawLine(x+1, y+1, x+sizeX/15-1, y+sizeY/15-1);
		//g.drawLine(x+1, y+sizeY/15-1, x+sizeX/15-1, y+1);
	}

	private void drawCircle(Graphics g, int x, int y) {
		Graphics2D g2= (Graphics2D) g;
		g.setColor(Color.LIGHT_GRAY);
		x=11+x*(View.WIDTH-37)/15;
		y=11+y*(View.HEIGHT-160)/15;
		g.fillOval(x+2, y+2, sizeX/15-4, sizeY/15-4);
		g.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(2));
		g.drawOval(x+2, y+2, sizeX/15-4, sizeY/15-4);
	}

	private void drawTriangle(Graphics g, int x, int y) {
		Graphics2D g2= (Graphics2D) g;
		g.setColor(Color.CYAN);
		x=11+x*(View.WIDTH-37)/15;
		y=11+y*(View.HEIGHT-160)/15;
		int xpoints[]={x+2, x+sizeX/30, x+sizeX/15-2};
		int ypoints[]={y+sizeY/15-2, y+2, y+sizeY/15-2};
		g.fillPolygon(xpoints,ypoints,3);
		g.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(2));
		g.drawPolygon(xpoints,ypoints,3);
	}

	protected void drawLog(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(5, sizeY+10, sizeX, 5+15*LINECOUNT);
		g.setFont(new Font("Courier", Font.PLAIN, 14));
		g.setColor(Color.BLACK);
		for(int y=0;y<log.size();y++){
			g.drawString(log.get(y), 10, sizeY+25+15*y);
		}
	}

	public void writeInLog(String text) {
		if(logLineCount>=LINECOUNT){
			log.remove(0);
		}
		log.add(text);
		logLineCount++;
	}

	public void setGrid(Grid grid) {
		this.grid=grid;
	}

	public Grid getGrid() {
		return this.grid;
	}
	public abstract String getNameOfTheGame();

}