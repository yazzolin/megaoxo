package megaoxo;


public class Laucher {

	public static void main(String[] args) {
		View window=new View(new TitleScreen());		
		window.setVisible(true);
	}
	public static void launchOxo(int numberOfPlayers ){
		View.WIDTH=500;
		View.HEIGHT=500;
		View window=new View(new OxoBoard());
		window.setVisible(true);
		window.changeType();
		GameOfOxo game=new GameOfOxo(window, numberOfPlayers);
		Mouse mouse =new Mouse(game);
		window.addMouseListener(mouse);
		game.start();
		
	}

}
