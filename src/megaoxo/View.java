package megaoxo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
//import java.awt.FlowLayout;
import javax.swing.JFrame;



@SuppressWarnings("serial")
public class View extends JFrame{
	private Board board;
	public static int WIDTH=500;
	public static int HEIGHT=500;
	
	public View(Board board){
		this.setLayout(new BorderLayout());
		this.setBoard(board);
		board.setPreferredSize(new Dimension(WIDTH,HEIGHT+100));
		//this.getContentPane().add(board);
		this.getContentPane().add(board);
		this.addComponentListener(new Resize());
		this.setTitle(board.getNameOfTheGame());
		this.setResizable(true);
		//this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBackground(Color.WHITE);
		this.pack();
	}
	

	/**
	 * @return the board
	 */
	public Board getBoard() {
		return board;
	}

	/**
	 * @param board2 the board to set
	 */
	public void setBoard(Board board2) {
		this.board = board2;
	}
	
	public void changeType(){
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

}
